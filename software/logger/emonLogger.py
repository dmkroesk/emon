#!/usr/bin/python

import RPi.GPIO as GPIO
import signal
import time
#from datetime import datetime
import MySQLdb
import logging

# Setup logger, using http://victorlin.me/posts/2012/08/26/good-logging-practice-in-python
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

handler = logging.FileHandler('emon.log')
handler.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

logger.addHandler(handler)

# Blinking led is connected to GPIO4 (#7)
PIN_KWHLED = 4

# Broadcom mode, pin is input. No pullup/down
# needed
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN_KWHLED, GPIO.IN)

# Global var's
start = time.time()

#
# callback, called when falling edge on PIN_KWHLED is detected
#
def cbKWHLed(channel):
	
	# Calculate instantaneous power usage (demand) in [W]
	global start
	now = time.time() 
	delta = now - start
	start = now
	ipu = (3600/delta);
	
	# update ipu in db
	try:
		query = """UPDATE kwhmeter SET ipu=%s WHERE k_id=%s"""
		cur.execute(query,(ipu,int(2)))	

		query = """INSERT INTO measurement (epoch, k_id) VALUES (%s, %s)"""
		cur.execute(query,(int(now),int(2)))

		db.commit()

		logger.debug('pulse detected, succesfully inserted epoch in db')

	except MySQLdb.Error as err:
		db.rollback()
		logger.error("%d: %s" % (err.args[0], err.args[1]));	

	# 		# update ipu in db
	# query = """UPDATE kwhmeter SET ipu=%s WHERE k_id=%s"""
	# try:
	# 	cur.execute(query,(ipu,int(2)))	
	# 	db.commit()
	# 	logger.info('IPU stored in db')
	# except:
	# 	db.rollback()
	# 	logger.error('can not write to db!')	

	# # store new epoch timetick (in UTC)
	# query = """INSERT INTO measurement (epoch, k_id) VALUES (%s, %s)"""
	# try:
	# 	cur.execute(query,(int(now),int(2)))	
	# 	db.commit()
	# 	logger.info('pulse detected, store timestamp in db')
	# except:
	# 	db.rollback()
	# 	logger.error('can not write to db!')

#
#
#
def ctrlc(sig, frame):
	global done 
	done = False


#
# Setup db
#
db = MySQLdb.connect(host="localhost",
			user="energy",
			passwd="energy2014",
			db="emon");
db.autocommit(True);
cur = db.cursor();

#
# Set falling edge callback
#
GPIO.add_event_detect(PIN_KWHLED, GPIO.FALLING, callback=cbKWHLed, bouncetime = 10)

#
# SIGINT
#
signal.signal(signal.SIGINT, ctrlc)

# 
# 'main'
#
logger.debug('Starting monitoring')

done = True;
while done != False:
	time.sleep(1)	
GPIO.remove_event_detect(PIN_KWHLED);
GPIO.cleanup();

logger.debug('Cleanup, bye')
